from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list
from datetime import datetime, timedelta
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.flood import stations_level_over_threshold
from floodsystem.analysis import future_prediction
from floodsystem.station import MonitoringStation
from floodsystem.utils import sorted_by_key

def run():
    stations = build_station_list()
    update_water_levels(stations)
    status = []
    tol = 1.5
    x = stations_level_over_threshold(stations, tol)    
    dt = 2

    for station in x:
        dates, levels = fetch_measure_levels(station[0].measure_id, dt= timedelta(days=dt))

        levels.reverse()
        rising = future_prediction(levels)
        level = MonitoringStation.relative_water_level(station[0])
        station_risk = level + 100*rising

        if station_risk > 10:
            station_status = 'Severe'
        elif station_risk > 5:
            station_status = 'High'
        elif station_risk > 1:
            station_status = 'Moderate'
        else:
            station_status = 'Low'

        status.append((station[0].name, station_status, station_risk))

    status.sort(key = lambda num: num[2], reverse = True)
    #statuses = [i[0:2] for i in status]

    for i in status:
        print (i[0] + ':', i[1], 'risk')

if __name__ == "__main__":
    run()
