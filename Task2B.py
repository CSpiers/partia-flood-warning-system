from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation

def run():
    stations = build_station_list()
    update_water_levels(stations)
    tol = 0.8
    x = stations_level_over_threshold(stations, tol)
    for i in x:
        print (i[0].name, i[1])
  

if __name__ == "__main__": 
    run()