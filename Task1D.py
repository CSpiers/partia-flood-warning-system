from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river


def run():
    stations = build_station_list()
    rivers = rivers_with_station(stations)
    rivers = list(rivers)
    rivers.sort()
    print(rivers[:10])

    rivers_stations = stations_by_river(stations)
    Aire = rivers_stations['River Aire']
    Cam = rivers_stations['River Cam']
    Thames = rivers_stations['River Thames']

    Aire.sort()
    Cam.sort()
    Thames.sort()
    print (Aire)
    print (Cam)
    print (Thames)


if __name__ == "__main__": 
    run()