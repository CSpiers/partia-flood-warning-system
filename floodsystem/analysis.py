from datetime import datetime, timedelta
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

def polyfit(dates, levels, p):
    x = matplotlib.dates.date2num(dates)
    y = levels

    # Using shifted x values, find coefficient of best-fit
    # polynomial f(x) of degree 4
    p_coeff = np.polyfit(x - x[0], y, p)

    # Convert coefficient into a polynomial that can be evaluated
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)

    return (poly, x[0])

def future_prediction(levels):
    y = levels
    if len(y) == 0:
        return 0
    elif len(y) < 10:
        return y[-1] - y[0]
    else:
        return y[-1] - y[-10]