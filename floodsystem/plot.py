import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import numpy as np
from floodsystem.analysis import polyfit

def plot_water_levels(station, dates, levels):
    
    plt.plot(dates, levels)
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation='vertical')
    plt.title(station.name)
    plt.axhline(y=station.typical_range[1], color='r', linestyle='--', label= 'typical high')
    plt.axhline(y=station.typical_range[0], color='g', linestyle='--', label= 'typical low')
    plt.legend()
    plt.tight_layout()

    return plt.show()


def plot_water_level_with_fit(station, dates, levels, p):
    x = matplotlib.dates.date2num(dates)
    plt.plot(dates, levels)
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation='vertical')
    plt.title(station.name)
    plt.axhline(y=station.typical_range[1], color='r', linestyle='--', label= 'typical high')
    plt.axhline(y=station.typical_range[0], color='g', linestyle='--', label= 'typical low')
    plt.legend()
    plt.tight_layout()

    poly, x[0] = polyfit(dates, levels, p)

    x1 = np.linspace(x[0], x[-1], 100)
    plt.plot(x1, poly(x1 - x[0]))

    return plt.show()