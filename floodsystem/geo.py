# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from floodsystem.utils import sorted_by_key  # noqa
from haversine import haversine, Unit

def stations_by_distance(stations, p):
    
    tuplist = []
    
    for station in stations:
        distance = haversine(station.coord, p)
        tuplist.append((station.name, station.town, distance))

    return sorted_by_key(tuplist, 2)

def stations_within_radius(stations, centre, r):
    
    tuplist = []
    
    for station in stations:
        x = haversine(station.coord, centre)
        if x <= r:
            tuplist.append(station.name)
   
    return tuplist
            
def rivers_with_station(stations):
    rivers = set()
    for station in stations:
        rivers.add(station.river)
    return rivers

def stations_by_river(stations):
    rivers_stations = {}
    for station in stations:
        if station.river in rivers_stations:
            lst = rivers_stations.get(station.river)
            lst.append(station.name)
            rivers_stations[station.river] = lst
        else:
            rivers_stations[station.river] = [station.name]
        
    return rivers_stations

def rivers_by_station_number(stations, N):
    rivers = []
    rivers_stations = stations_by_river(stations)
    for river in rivers_stations:
        lst = rivers_stations.get(river)
        num = len(lst)
        rivers.append((river,num))

    rivers.sort(key = lambda num: num[1], reverse = True)
    N_rivers = rivers[:N]
    Nth_river_num = rivers[N-1][1]

    for i in range(len(rivers) - N):
        if rivers[N + i][1] == Nth_river_num:
            N_rivers.append(rivers[N+i])
        else:
            return N_rivers