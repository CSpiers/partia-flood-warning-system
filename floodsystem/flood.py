from floodsystem.utils import sorted_by_key
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation

def stations_level_over_threshold(stations, tol):
    tuplist = []
    for station in stations:
        if MonitoringStation.typical_range_consistent(station) == True:
            if station.latest_level != None:
                if MonitoringStation.relative_water_level(station) > tol:
                    tuplist.append((station, MonitoringStation.relative_water_level(station)))
    
    return sorted_by_key(tuplist, 1, reverse= True)

def stations_highest_rel_level(stations, N):
    proplist = []
    for station in stations:
        if MonitoringStation.typical_range_consistent(station) == True:
            if station.latest_level != None:
                proplist.append([station, MonitoringStation.relative_water_level(station)])
            
    return sorted_by_key(proplist, 1, reverse= True)[0:N]