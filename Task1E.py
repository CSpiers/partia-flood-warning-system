from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number

def run():
    stations = build_station_list()

    print (rivers_by_station_number(stations, 5))


if __name__ == "__main__": 
    run()
