from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.flood import stations_highest_rel_level

x = MonitoringStation(None, None, 'Station 1', (30, 50), (0.30, 0.70), 'River 1', 'Town 1')
y = MonitoringStation(None, None, 'Station 2', (20, 30), (0.45, 0.98), 'River 2', 'Town 2')
z = MonitoringStation(None, None, 'Station 3', (10, 20), (0.12, 0.76), 'River 3', 'Town 3')
x.latest_level = 0.4
y.latest_level = 0.98
z.latest_level = 0.44
sample = [x, y, z]

def test_stations_level_over_threshhold():
    stations = sample
    tol = 0.3
    result = stations_level_over_threshold(stations, tol)

    assert len(result) == 2
    assert result[0][1] == 1.0
    assert result[1][1] == 0.5

def test_stations_highest_rel_level():
    stations = sample
    N = 3
    outcome = stations_highest_rel_level(stations, N)

    assert len(outcome) == 3
    assert outcome[0][1] == 1.0
    assert outcome[2][1] == 0.2500000000000001