from floodsystem.geo import stations_by_distance
from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number


def test_stations_by_distance():
    stations = build_station_list()[0:3]
    p = (52.2053, 0.1218)

    x = stations_by_distance(stations, p)
    
    assert len(x) == 3
    assert x[0][0] == 'Surfleet Sluice'
    assert x[2][0] == 'Gaw Bridge'

def test_stations_within_radius():
    stations = build_station_list()[0:3]
    centre = (52.2053, 0.1218)
    r = 10

    x = stations_within_radius(stations, centre, r)

    assert type(x) == list
    assert x == []

def test_rivers_with_station():
    stations = build_station_list()

    x = rivers_with_station(stations)

    assert type(x) == set

def test_stations_by_river():
    stations = build_station_list()

    x = stations_by_river(stations)

    assert type(x) == dict

def test_rivers_by_station_number():
    stations = build_station_list()
    N = 10
    x = rivers_by_station_number(stations, N)

    assert type(x) == list