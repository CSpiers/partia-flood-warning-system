from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation

def run():
    stations = build_station_list()
    update_water_levels(stations)
    N = 10
    x = stations_highest_rel_level(stations, N)
    for i in x:
        print (i[0].name, i[1])
  

if __name__ == "__main__": 
    run()